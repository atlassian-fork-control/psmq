package com.atlassian.psmq.internal.bootstrap;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.psmq.api.QSessionFactory;

/**
 */
@Scanned
public class OsgiImports
{
    @ComponentImport
    QSessionFactory qSessionFactory;
    @ComponentImport
    EventPublisher eventPublisher;
}
