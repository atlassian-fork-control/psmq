package com.atlassian.psmq.internal.rest.representations;

import com.atlassian.psmq.api.message.QMessage;
import com.atlassian.psmq.internal.util.rest.RestKit;

import java.util.Collection;

/**
 */
@SuppressWarnings ({ "FieldCanBeLocal", "UnusedDeclaration" })
public class MessageNoBufferDTO
{
    private final String messageId;
    private final String createDate;
    private final String expiryDate;
    private final String contentType;
    private final int contentVersion;
    private final String priority;
    private final long contentLength;
    private final Collection<PropertyDTO> properties;

    public MessageNoBufferDTO(QMessage message)
    {
        this.messageId = message.messageId().value();
        this.createDate = RestKit.isoDate(message.systemMetaData().createdDate());
        this.expiryDate = RestKit.isoDate(message.expiryDate().orElse(null));
        this.contentType = message.contentType().value();
        this.contentVersion = message.contentVersion().value();
        this.priority = message.priority().displayName();
        this.contentLength = message.buffer().length();
        this.properties = PropertyDTO.of(message.properties());
    }
}
