package com.atlassian.psmq.internal.rest.representations;

import com.atlassian.psmq.api.message.QMessage;

/**
 */
@SuppressWarnings ({ "FieldCanBeLocal", "UnusedDeclaration" })
public class MessageDTO extends MessageNoBufferDTO
{
    private final String content;

    public MessageDTO(QMessage message)
    {
        super(message);
        this.content = message.buffer().asString();
    }

}
