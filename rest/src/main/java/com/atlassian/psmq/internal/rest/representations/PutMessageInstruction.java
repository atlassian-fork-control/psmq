package com.atlassian.psmq.internal.rest.representations;

import com.atlassian.fugue.Option;
import com.atlassian.psmq.api.message.QContentType;
import com.atlassian.psmq.api.message.QContentVersion;
import com.atlassian.psmq.api.message.QMessagePriority;

import java.util.Date;

import static com.atlassian.psmq.internal.util.rest.RestKit.parseIsoDate;

/**
 * A put message instruction
 */
@SuppressWarnings ("UnusedDeclaration")
public class PutMessageInstruction
{
    private String contentType;
    private String expiryDate;
    private Integer priority;
    private Integer contentVersion;
    private String content;

    public Option<String> getContent()
    {
        return Option.option(content);
    }

    public Option<QContentType> getContentType()
    {
        return Option.option(contentType).map(ct -> new QContentType(ct));
    }

    public Option<QMessagePriority> getPriority()
    {
        return Option.option(priority).map(p -> new QMessagePriority(p));
    }

    public Option<QContentVersion> getContentVersion()
    {
        return Option.option(contentVersion).map(v -> new QContentVersion(v));
    }

    public Option<Date> getExpiryDate()
    {
        return Option.option(expiryDate).map(s -> parseIsoDate(s).get());
    }
}
