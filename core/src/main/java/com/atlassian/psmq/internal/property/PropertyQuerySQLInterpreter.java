package com.atlassian.psmq.internal.property;

import com.atlassian.psmq.api.internal.Validations;
import com.atlassian.psmq.api.property.query.PropertyExpressions;
import com.atlassian.psmq.api.property.query.QPropertyQuery;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.core.types.dsl.StringPath;

import javax.annotation.Nullable;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * This converts a "ideal" logical expression into a QueryDSL logical expression using the database columns and QueryDSL
 * constructs.  If you add new capabilities in the "ideal" layer you have to add its peer code here in terms of
 * QueryDSL
 *
 * @since v1.0
 */
public class PropertyQuerySQLInterpreter {
    final StringPath NAME_COL;
    final StringPath STRING_VALUE_COL;
    final NumberPath<Long> LONG_VALUE_COL;

    public PropertyQuerySQLInterpreter(final PropertyColumnProvider columnProvider) {
        this.NAME_COL = columnProvider.getPropertyNameColumn();
        this.LONG_VALUE_COL = columnProvider.getPropertyLongValueColumn();
        this.STRING_VALUE_COL = columnProvider.getPropertyStringValueColumn();
    }

    public Predicate convert(QPropertyQuery query) {
        checkNotNull(query);

        QPropertyQuery.Operator operator = query.getOperator();
        if (operator == QPropertyQuery.Operator.AND) {
            return new BooleanBuilder(asLogicalExpr(query.getLeft())).and(asLogicalExpr(query.getRight()));
        }
        if (operator == QPropertyQuery.Operator.OR) {
            return new BooleanBuilder(asLogicalExpr(query.getLeft())).or(asLogicalExpr(query.getRight()));
        }

        // compare against msg property names from here on in.  Strong assumption here is that
        // the left hand side is ALWAYS a property name because that's how we let people
        // build them.  This is really important.  We have a NAME_COL / VAL_COL paradigm
        // in properties and we must preserve that.

        PropertyExpressions.PropertyExpression propertyNameExpr = query.getLeft();
        Validations.checkState(propertyNameExpr instanceof PropertyExpressions.StringConstant);

        BooleanExpression msgPropertyName = NAME_COL.eq(convertStr(propertyNameExpr));

        if (operator == QPropertyQuery.Operator.IS_NOT_NULL) {
            return msgPropertyName.isNotNull();
        }
        if (operator == QPropertyQuery.Operator.IS_NULL) {
            return msgPropertyName.isNull();
        }
        //
        // ok must take a right hand side
        return new BooleanBuilder(msgPropertyName).and(valuePredicate(operator, query.getRight()));
    }

    private Predicate asLogicalExpr(PropertyExpressions.PropertyExpression<?> expression) {
        Validations.checkArgument(expression instanceof QPropertyQuery, "AND/ORs must be LogicalExpression's");
        //noinspection ConstantConditions
        return convert((QPropertyQuery) expression);
    }

    private Predicate valuePredicate(final QPropertyQuery.Operator operator, final PropertyExpressions.PropertyExpression right) {
        if (right instanceof PropertyExpressions.StringConstant) {
            return stringOpVisitor(operator).visit((PropertyExpressions.StringConstant) right, STRING_VALUE_COL);
        }
        if (right instanceof PropertyExpressions.LongConstant) {
            return longOpVisitor(operator).visit((PropertyExpressions.LongConstant) right, LONG_VALUE_COL);
        }
        throw new UnsupportedOperationException("Not implemented");
    }

    /**
     * If you add more operations in the logical model, then add them into the physical model here as peer code
     *
     * @param operator the operator of choice
     * @return a visitor that will find the sql predicate to use
     */
    private PropertyExpressions.Visitor<Predicate, NumberPath<Long>> longOpVisitor(final QPropertyQuery.Operator operator) {
        return new PropertyExpressions.Visitor<Predicate, NumberPath<Long>>() {
            @Override
            public Predicate visit(final PropertyExpressions.LongConstant expr, final NumberPath<Long> context) {
                Long l = expr.getValue();
                if (operator == QPropertyQuery.Operator.EQ) {
                    return context.eq(l);
                } else if (operator == QPropertyQuery.Operator.NE) {
                    return context.ne(l);
                } else if (operator == QPropertyQuery.Operator.GOE) {
                    return context.goe(l);
                } else if (operator == QPropertyQuery.Operator.GT) {
                    return context.gt(l);
                } else if (operator == QPropertyQuery.Operator.LOE) {
                    return context.loe(l);
                } else if (operator == QPropertyQuery.Operator.LT) {
                    return context.lt(l);
                }
                throw new UnsupportedOperationException("Not implemented");
            }

            @Override
            public Predicate visit(final PropertyExpressions.StringConstant expr, final NumberPath<Long> context) {
                throw new UnsupportedOperationException("Not implemented");
            }
        };
    }

    /**
     * If you add more string operations in the logical model, then add them into the physical model here as peer code
     *
     * @param operator the operator of choice
     * @return a visitor that will find the sql predicate to use
     */
    private PropertyExpressions.Visitor<Predicate, StringPath> stringOpVisitor(final QPropertyQuery.Operator operator) {
        return new PropertyExpressions.Visitor<Predicate, StringPath>() {
            @Override
            public Predicate visit(final PropertyExpressions.LongConstant expr, final StringPath context) {
                throw new UnsupportedOperationException("Not implemented");
            }

            @Nullable
            @Override
            public Predicate visit(final PropertyExpressions.StringConstant expr, final StringPath context) {
                String value = expr.getValue();
                if (operator == QPropertyQuery.Operator.EQ) {
                    return context.eq(value);
                } else if (operator == QPropertyQuery.Operator.NE) {
                    return context.ne(value);
                } else if (operator == QPropertyQuery.Operator.EQ_IGNORE_CASE) {
                    return context.equalsIgnoreCase(value);
                } else if (operator == QPropertyQuery.Operator.CONTAINS) {
                    return context.contains(value);
                } else if (operator == QPropertyQuery.Operator.STARTS_WITH) {
                    return context.startsWith(value);
                } else if (operator == QPropertyQuery.Operator.ENDS_WITH) {
                    return context.endsWith(value);
                } else if (operator == QPropertyQuery.Operator.LIKE) {
                    return context.like(value);
                }
                throw new UnsupportedOperationException("Not implemented");
            }
        };
    }

    private String convertStr(final PropertyExpressions.PropertyExpression strExpression) {
        PropertyExpressions.Visitor<String, Void> v = new PropertyExpressions.Visitor<String, Void>() {
            @Nullable
            @Override
            public String visit(final PropertyExpressions.LongConstant expr, @Nullable final Void context) {
                throw new UnsupportedOperationException("Not implemented");
            }

            @Nullable
            @Override
            public String visit(final PropertyExpressions.StringConstant expr, @Nullable final Void context) {
                return expr.getValue();
            }
        };
        return v.visit((PropertyExpressions.StringConstant) strExpression, null);
    }

    /**
     * This allows you to map the DB column names of the actual property columns.
     */
    public interface PropertyColumnProvider {
        StringPath getPropertyNameColumn();

        StringPath getPropertyStringValueColumn();

        NumberPath<Long> getPropertyLongValueColumn();
    }

}
