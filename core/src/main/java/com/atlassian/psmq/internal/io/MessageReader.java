package com.atlassian.psmq.internal.io;

import com.atlassian.fugue.Option;
import com.atlassian.psmq.api.QException;
import com.atlassian.psmq.api.message.QMessage;
import com.atlassian.psmq.api.message.QMessageQuery;
import com.atlassian.psmq.internal.io.db.MessageDbTO;
import com.atlassian.psmq.internal.io.db.QueueDao;
import com.atlassian.psmq.internal.queue.QueueImpl;

import java.util.concurrent.TimeUnit;

import static com.atlassian.psmq.api.internal.Validations.checkNotNull;
import static com.atlassian.psmq.api.internal.Validations.checkState;
import static java.lang.String.format;

/**
 * Responsible for reading messages from the database and assembling them into QMessage format
 */
public class MessageReader {
    public static final long FAIL_SAFE_MAX_TIME_MS = TimeUnit.MILLISECONDS.convert(10, TimeUnit.SECONDS);
    private final QueueDao queueDao;

    public MessageReader(final QueueDao queueDao) {
        this.queueDao = queueDao;
    }

    public Option<QMessage> claimAndResolve(SessionInstructions instructions, QMessageQuery query) throws QException {
        return claimImpl(instructions, query, true);
    }

    public Option<QMessage> claim(final SessionInstructions instructions, final QMessageQuery query) {
        return claimImpl(instructions, query, false);
    }

    private Option<QMessage> claimImpl(
            final SessionInstructions instructions,
            final QMessageQuery query,
            boolean resolveItImmediately) {
        checkNotNull(instructions);
        checkNotNull(query);

        final QueueImpl queue = instructions.queue().get();
        final long queueId = queue.id().value();
        String generalErrorMsg = format("Unable to read from queue '%s'", queue.name());

        TxnBoundary txn = instructions.txnBoundary();
        return txn.run(generalErrorMsg, txnContext -> claimFromQueue(txnContext, query, resolveItImmediately, queueId));
    }

    private Option<QMessage> claimFromQueue(
            final TxnContext txnContext,
            final QMessageQuery query,
            final boolean resolveItImmediately,
            final long queueId) {
        // keep reading until we find a non expired messages
        // or we run out of messages
        long then = System.currentTimeMillis();
        while (true) {
            queueDao.heartBeatQueue(txnContext, queueId);

            //
            // if we can see messages in the queue but they are all claimed then we have a contention / completion
            // problem.  Rather than go into an infinite loop, lets back off and say there are no messages because
            // within a reasonable time, there is none!
            //
            if ((System.currentTimeMillis() - then) > FAIL_SAFE_MAX_TIME_MS) {
                return Option.none();
            }

            Option<MessageDbTO> nextMsg = queueDao.nextMsg(txnContext, queueId, query);
            if (nextMsg.isEmpty()) {
                return Option.none();
            }

            // ok we have a message
            MessageDbTO msg = nextMsg.get();
            boolean claimed = queueDao.claimMsg(txnContext, queueId, msg.getId());

            // did we claim it?  if we didn't get it then some other thread snagged if before we had a
            // chance so its a read miss.  Go around again
            if (!claimed) {
                continue;
            }

            // delete the message if it's expired, and try to claim a new one
            if (isExpired(msg)) {
                queueDao.deleteMsg(txnContext, queueId, msg.getId());
                continue;
            }

            if (resolveItImmediately) {
                queueDao.deleteMsg(txnContext, queueId, msg.getId());
            }
            return nextMsg.map(msgDef -> msgDef.toMessage(txnContext.claimant()));
        }
    }

    public void resolve(final SessionInstructions instructions, final QMessage message) {
        resolveOrUnresolveImpl(instructions, message, ResolveAction.RESOLVE);
    }

    public void unresolve(final SessionInstructions instructions, final QMessage message) {
        resolveOrUnresolveImpl(instructions, message, ResolveAction.UNRESOLVE);
    }

    private void resolveOrUnresolveImpl(final SessionInstructions instructions, final QMessage message, final ResolveAction resolveAction) {
        checkNotNull(instructions);
        checkNotNull(message);


        QueueImpl queue = instructions.queue().get();
        String generalErrorMsg = format("Unable to resolve in queue '%s'", queue.name());

        TxnBoundary txn = instructions.txnBoundary();
        txn.run(generalErrorMsg, txnContext -> {
            //
            // check that the message is suitable for resolution actions - ie it was originally claimed by this session
            checkState(message.systemMetaData().claimant().equals(txnContext.claimant()), "You can only resolve messages that you claimed with this session");

            long queueId = queue.id().value();
            long msgId = message.systemMetaData().systemId().value();
            if (resolveAction == ResolveAction.RESOLVE) {
                queueDao.deleteMsg(txnContext, queueId, msgId);
            } else {
                queueDao.unresolveMsg(txnContext, queueId, msgId);
            }
            queueDao.heartBeatQueue(txnContext, queueId);

            return true;
        });
    }

    private boolean isExpired(final MessageDbTO nextMsg) {
        Option<Long> expiry = nextMsg.getExpiryTime();
        long now = System.currentTimeMillis();
        return expiry.exists(t -> now > t);
    }

    private enum ResolveAction {
        RESOLVE, UNRESOLVE
    }
}
