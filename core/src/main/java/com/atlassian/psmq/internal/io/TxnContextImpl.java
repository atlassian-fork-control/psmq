package com.atlassian.psmq.internal.io;

import com.atlassian.pocketknife.api.querydsl.DatabaseConnection;
import com.atlassian.psmq.api.message.QClaimant;

public class TxnContextImpl implements TxnContext {
    final QClaimant claimant;
    final DatabaseConnection connection;
    final long claimantHeartBeat;

    public TxnContextImpl(final DatabaseConnection connection, final QClaimant claimant, final long claimantHeartBeat) {
        this.connection = connection;
        this.claimant = claimant;
        this.claimantHeartBeat = claimantHeartBeat;
    }

    @Override
    public DatabaseConnection connection() {
        return connection;
    }

    @Override
    public QClaimant claimant() {
        return claimant;
    }

    @Override
    public long claimantHeartBeatMillis() {
        return claimantHeartBeat;
    }
}
