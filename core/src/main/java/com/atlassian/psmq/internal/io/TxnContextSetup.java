package com.atlassian.psmq.internal.io;

import com.atlassian.fugue.Option;
import com.atlassian.psmq.api.message.QClaimant;

import java.sql.Connection;

/**
 * A TxnContext is passed into code that is taking part in a Txn
 */
public interface TxnContextSetup {
    /**
     * @return the connection to use for this Txn work
     */
    Option<Connection> connection();

    /**
     * @return the claimant info in force for this session
     */
    QClaimant claimant();

    /**
     * @return the claimant heart beat millis
     */
    long claimantHeartBeatMillis();
}
