package com.atlassian.psmq.internal.message;

import com.atlassian.psmq.api.QException;
import com.atlassian.psmq.api.message.QMessage;
import com.atlassian.psmq.api.message.QMessageConsumer;
import com.atlassian.psmq.api.message.QMessageQuery;
import com.atlassian.psmq.api.message.QMessageQueryBuilder;
import com.atlassian.psmq.internal.QCloseableImpl;
import com.atlassian.psmq.internal.io.MessageReader;
import com.atlassian.psmq.internal.io.SessionInstructions;

import java.util.Optional;

import static com.atlassian.psmq.internal.util.HelperKit.toOptional;
import static java.lang.String.format;

/**
 */
public class QMessageConsumerImpl extends QCloseableImpl implements QMessageConsumer {
    private final MessageReader messageReader;
    private final SessionInstructions instructions;


    public QMessageConsumerImpl(final MessageReader messageReader, final SessionInstructions instructions) {
        this.instructions = instructions;
        this.messageReader = messageReader;
    }

    @Override
    public Optional<QMessage> claimAndResolve() throws QException {
        return claimAndResolve(QMessageQueryBuilder.anyMessage());
    }

    @Override
    public Optional<QMessage> claimAndResolve(final QMessageQuery query) throws QException {
        checkNotClosed(format("This message consumer is closed : '%s'", instructions.queue().get().name()));
        return toOptional(messageReader.claimAndResolve(instructions, query));
    }


    @Override
    public Optional<QMessage> claimMessage() throws QException {
        return claimMessage(QMessageQueryBuilder.anyMessage());
    }

    @Override
    public Optional<QMessage> claimMessage(final QMessageQuery query) throws QException {
        checkNotClosed(format("This message consumer is closed : '%s'", instructions.queue().get().name()));
        return toOptional(messageReader.claim(instructions, query));
    }

    @Override
    public void resolveClaimedMessage(final QMessage message) {
        checkNotClosed(format("This message consumer is closed : '%s'", instructions.queue().get().name()));
        messageReader.resolve(instructions, message);
    }

    @Override
    public void unresolveClaimedMessage(final QMessage message) {
        checkNotClosed(format("This message consumer is closed : '%s'", instructions.queue().get().name()));
        messageReader.unresolve(instructions, message);
    }
}
