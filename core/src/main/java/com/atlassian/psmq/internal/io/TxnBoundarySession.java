package com.atlassian.psmq.internal.io;

import com.atlassian.pocketknife.api.querydsl.DatabaseConnection;
import com.atlassian.pocketknife.api.querydsl.DatabaseConnectionConverter;
import com.google.common.base.Function;

import static com.atlassian.psmq.api.internal.Validations.checkState;

/**
 */
public class TxnBoundarySession implements TxnBoundary {
    private final DatabaseConnectionConverter databaseConnectionConverter;
    private final TxnFixture txnFixture;

    public TxnBoundarySession(final TxnFixture txnFixture, DatabaseConnectionConverter databaseConnectionConverter) {
        this.txnFixture = txnFixture;
        this.databaseConnectionConverter = databaseConnectionConverter;
    }

    @Override
    public <T> T run(final String operationMessage, final Function<TxnContext, T> txnFunction) {
        TxnContextSetup txnContextSetup = txnFixture.preTxn();
        TxnContext txnContext = makeTxnCtx(txnContextSetup);
        try {
            T result = txnFunction.apply(txnContext);
            //
            // auto closeable results (such as Streams) cant have the connection returned on
            // the Txn boundary because there are not finished yet in consumption terms
            // instead the onClose() mechanism has to be used and the TxnContext close Runnable
            // invoked
            //
            boolean isAutoCloseable = result instanceof AutoCloseable;
            if (!isAutoCloseable) {
                txnFixture.onTxnSuccess(txnContext);
            }
            return result;
        } catch (RuntimeException rte) {
            txnFixture.onTxnException(txnContext);
            throw rte;
        }
    }

    private TxnContext makeTxnCtx(TxnContextSetup txnContextSetup) {
        checkState(txnContextSetup.connection().isDefined());

        DatabaseConnection databaseConnection = databaseConnectionConverter.convert(txnContextSetup.connection().get());
        return new TxnContextImpl(databaseConnection, txnContextSetup.claimant(), txnContextSetup.claimantHeartBeatMillis());
    }
}
