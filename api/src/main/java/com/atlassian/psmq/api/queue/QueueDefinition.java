package com.atlassian.psmq.api.queue;

import com.atlassian.annotations.PublicApi;
import com.atlassian.psmq.api.property.QProperty;

import java.util.Optional;
import java.util.Set;

/**
 * The definition of a queue
 */
@PublicApi
public interface QueueDefinition
{
    /**
     * @return the intended name of the queue
     */
    String name();

    /**
     * @return the intended purpose of the queue
     */
    String purpose();

    /**
     * @return an optional topic associated with the queue
     */
    Optional<QTopic> topic();

    /**
     * @return the properties associated with this queue
     */
    Set<QProperty> properties();

}
