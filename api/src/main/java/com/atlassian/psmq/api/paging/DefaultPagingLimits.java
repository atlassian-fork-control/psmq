package com.atlassian.psmq.api.paging;

import com.atlassian.annotations.Internal;

/**
 * Paging has limits and here they are
 */
@Internal
public class DefaultPagingLimits
{
    /**
     * The maximum number of queue entries that will ever by returned
     */
    public static final int MAX_QUEUES_RETURNED = 100;

    /**
     * The default number of queue entries that will be returned by default
     */
    public static final int DEFAULT_QUEUES_RETURNED = 20;

    /**
     * The maximum number of messages that will ever by returned
     */
    public static final int MAX_MESSAGES_RETURNED = 100;

    /**
     * The default number of messages that will be returned by default
     */
    public static final int DEFAULT_MESSAGES_RETURNED = 20;

}
