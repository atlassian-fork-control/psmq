package com.atlassian.psmq.api.message;

import com.atlassian.annotations.PublicApi;
import com.atlassian.psmq.api.property.QPropertySet;

import java.util.Date;
import java.util.Optional;

/**
 * A {@link com.atlassian.psmq.api.message.QMessage} is the data you put in {@link com.atlassian.psmq.api.queue.Queue}s
 *
 * @since v1.0
 */
@PublicApi
public interface QMessage
{
    /**
     * @return the associated system generated meta data about this message
     */
    QMessageSystemMetaData systemMetaData();

    /**
     * @return the message id
     */
    QMessageId messageId();

    /**
     *
     * @return (optionally) the date the message will expire
     */
    Optional<Date> expiryDate();

    /**
     * @return true if the messages has expired
     */
    boolean expired();

    /**
     * @return the priority of the message
     */
    QMessagePriority priority();

    /**
     *
     * @return the content type of the message
     */
    QContentType contentType();

    /**
     * @return the version of the message content
     */
    QContentVersion contentVersion();

    /**
     * @return the data of the message
     */
    QBuffer buffer();

    /**
     * @return the properties associated with this message
     */
    QPropertySet properties();
}
