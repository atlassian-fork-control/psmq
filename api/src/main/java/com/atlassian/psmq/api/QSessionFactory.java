package com.atlassian.psmq.api;

import com.atlassian.annotations.PublicApi;

/**
 * This is the starting place for PSMQ
 */
@PublicApi
public interface QSessionFactory
{
    /**
     * You create a PSMQ session by specifying how you want it to act
     *
     * @param sessionDefinition the definition of the PSMQ session
     *
     * @return the new session
     *
     * @throws QException in exceptional circumstances
     */
    QSession createSession(QSessionDefinition sessionDefinition) throws QException;
}
