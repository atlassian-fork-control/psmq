package com.atlassian.psmq.api.message;

import com.atlassian.annotations.PublicApi;
import com.atlassian.psmq.api.property.QProperty;
import com.atlassian.psmq.api.property.QPropertyBuilder;
import com.atlassian.psmq.api.property.QPropertySet;
import com.atlassian.psmq.api.property.QPropertySetBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.Date;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static com.atlassian.psmq.api.internal.Validations.checkNotNull;
import static com.atlassian.psmq.api.internal.Validations.checkState;

/**
 * A builder of {@link com.atlassian.psmq.api.message.QMessage}s.
 *
 * @since v1.0
 */
@PublicApi
public class QMessageBuilder {
    QMessageSystemMetaData systemMetaData;
    QMessageId messageId;
    Optional<Date> expiry;
    QMessagePriority priority;
    QContentType contentType;
    QContentVersion contentVersion;
    QBuffer buffer;
    Set<QProperty> properties;
    boolean built = false;

    public QMessageBuilder() {
        systemMetaData = genesisMetaData();
        messageId = new QMessageId();
        expiry = Optional.empty();
        contentType = QContentType.PLAIN_TEXT;
        contentVersion = QContentVersion.UNSPECIFIED;
        priority = QMessagePriority.NORMAL;
        buffer = QBufferBuilder.newBuffer().build();
        properties = new HashSet<>();
    }

    public static QMessageBuilder newMsg() {
        return new QMessageBuilder();
    }

    private QMessageSystemMetaData genesisMetaData() {
        final QSystemId systemId = new QSystemId(-1);
        final QClaimant claimant = new QClaimant();
        final Date created = new Date();

        return new QMessageSystemMetaData() {
            @Override
            public QSystemId systemId() {
                return systemId;
            }

            @Override
            public QClaimant claimant() {
                return claimant;
            }

            @Override
            public Date createdDate() {
                return created;
            }

            @Override
            public int claimCount() {
                return 0;
            }
        };
    }


    /**
     * A creator of message is not expected to ever set this.
     *
     * @param systemMetaData the system meta data to use
     *
     * @return the builder
     */
    public QMessageBuilder withSystemMetaData(QMessageSystemMetaData systemMetaData) {
        this.systemMetaData = checkNotNull(systemMetaData);
        return this;
    }

    public QMessageBuilder withId(QMessageId messageId) {
        this.messageId = checkNotNull(messageId);
        return this;
    }

    public QMessageBuilder withContentType(QContentType contentType) {
        this.contentType = checkNotNull(contentType);
        return this;
    }

    public QMessageBuilder withContentVersion(QContentVersion contentVersion) {
        this.contentVersion = checkNotNull(contentVersion);
        return this;
    }

    public QMessageBuilder withExpiry(Date expiry) {
        this.expiry = Optional.of(expiry);
        return this;
    }

    public QMessageBuilder withExpiryFromNow(long ms) {
        this.expiry = Optional.of(new Date(System.currentTimeMillis() + ms));
        return this;
    }

    public QMessageBuilder withExpiry(Optional<Date> expiry) {
        this.expiry = expiry;
        return this;
    }

    public QMessageBuilder withPriority(QMessagePriority priority) {
        this.priority = checkNotNull(priority);
        return this;
    }

    public QBufferBuilder newBuffer() {
        return QBufferBuilder.newBuffer(this);
    }

    public QMessageBuilder withBuffer(QBuffer buffer) {
        this.buffer = checkNotNull(buffer);
        return this;
    }

    public QMessageBuilder withProperty(String name, String value) {
        this.properties.add(prop().with(name, value).build());
        return this;
    }

    public QMessageBuilder withProperty(String name, long value) {
        this.properties.add(prop().with(name, value).build());
        return this;
    }

    public QMessageBuilder withProperty(String name, boolean value) {
        this.properties.add(prop().with(name, value).build());
        return this;
    }

    public QMessageBuilder withProperty(String name, Date value) {
        this.properties.add(prop().with(name, value).build());
        return this;
    }

    public QMessageBuilder withProperty(QProperty property) {
        this.properties.add(checkNotNull(property));
        return this;
    }

    public QMessageBuilder withProperties(Set<QProperty> properties) {
        checkNotNull(properties).forEach(this::withProperty);
        return this;
    }

    public QMessageBuilder withProperties(QPropertySet properties) {
        Set<QProperty> qProperties = checkNotNull(properties).asSet();
        checkNotNull(qProperties).forEach(this::withProperty);
        return this;
    }

    private QPropertyBuilder prop() {
        return QPropertyBuilder.newProperty();
    }

    public QMessage build() {
        checkState(!built, "A QMessageBuilder can only be used once to build a QMessage.  Create a new builder per message");
        return new QMessageImpl(properties);
    }


    private class QMessageImpl implements QMessage {
        private final QPropertySet properties;

        private QMessageImpl(Set<QProperty> properties) {
            this.properties = QPropertySetBuilder.newPropertySet(properties);
        }

        @Override
        public QMessageSystemMetaData systemMetaData() {
            return systemMetaData;
        }

        @Override
        public QMessageId messageId() {
            return messageId;
        }

        @Override
        public Optional<Date> expiryDate() {
            return expiry;
        }

        @Override
        public boolean expired() {
            return expiryDate().map(d -> d.getTime() > System.currentTimeMillis()).orElse(false);
        }

        @Override
        public QMessagePriority priority() {
            return priority;
        }

        @Override
        public QContentType contentType() {
            return contentType;
        }

        @Override
        public QContentVersion contentVersion() {
            return contentVersion;
        }

        @Override
        public QBuffer buffer() {
            return buffer;
        }

        @Override
        public QPropertySet properties() {
            return properties;
        }

        @Override
        public String toString() {
            return new ToStringBuilder(this)
                    .append("systemId", systemMetaData.systemId())
                    .append("messageId", messageId())
                    .append("claimant", systemMetaData.claimant())
                    .append("created", systemMetaData.createdDate())
                    .append("expired", expired())
                    .append("contentType", contentType().value())
                    .append("version", contentVersion().value())
                    .append("priority", priority().value())
                    .append("length", buffer.length())
                    .toString();
        }
    }
}
