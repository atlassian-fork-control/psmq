package com.atlassian.psmq.api;

import com.atlassian.annotations.PublicApi;

/**
 * QExceptions happen in exceptional circumstances and are thrown by many API methods
 */
@PublicApi
public class QException extends RuntimeException
{
    public QException()
    {
        super();
    }

    public QException(final Throwable cause)
    {
        super(cause);
    }

    public QException(final String message)
    {
        super(message);
    }

    public QException(final String message, final Throwable cause)
    {
        super(message, cause);
    }
}
