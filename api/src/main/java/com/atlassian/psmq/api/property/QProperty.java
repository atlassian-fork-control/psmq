package com.atlassian.psmq.api.property;

import com.atlassian.annotations.PublicApi;

import java.util.Date;

/**
 * A queue or message can have a set of properties associated with it
 *
 * @since v1.0
 */
@PublicApi
public interface QProperty
{
    /**
     * A message property can be of the following types
     */
    enum Type
    {
        STRING, LONG, DATE, BOOLEAN
    }

    /**
     * @return the type of the property
     */
    Type type();

    /**
     * @return the property name
     */
    String name();

    /**
     * @return the value of the property as a String
     * @throws java.lang.IllegalStateException if this underlying type is not {@link
     * QProperty.Type#STRING}
     */
    String stringValue();

    /**
     * @return the value of the property as a long
     * @throws java.lang.IllegalStateException if this underlying type is not {@link
     * QProperty.Type#LONG}
     */
    long longValue();

    /**
     * @return the value of the property as a boolean
     * @throws java.lang.IllegalStateException if this underlying type is not {@link
     * QProperty.Type#BOOLEAN}
     */
    boolean booleanValue();

    /**
     * @return the value of the property as a Date
     * @throws java.lang.IllegalStateException if this underlying type is not {@link
     * QProperty.Type#DATE}
     */
    Date dateValue();

    /**
     * This allows you to get the underlying value as a String without any type safety.
     *
     * @return a string appropriate approximation of the property value
     */
    String asString();
}
