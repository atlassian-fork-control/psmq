package com.atlassian.psmq.api.message;

import com.atlassian.annotations.PublicApi;
import com.atlassian.psmq.api.QException;

import java.util.Optional;

/**
 * A consumer of {@link com.atlassian.psmq.api.message.QMessage}s that claims them from a {@link com.atlassian.psmq.api.queue.Queue}
 *
 * @since v1.0
 */
@PublicApi
public interface QMessageConsumer extends QCloseable {
    /**
     * This will claim a message from the underlying {@link com.atlassian.psmq.api.queue.Queue} and immediately resolve it as
     * processed.
     *
     * @return (optionally) a read message or none()
     *
     * @throws QException in exceptional circumstances
     */
    Optional<QMessage> claimAndResolve() throws QException;

    /**
     * This will claim a message from the underlying {@link com.atlassian.psmq.api.queue.Queue} and immediately resolve it as
     * processed.
     *
     * @param query this is criteria for claiming messages in the queue
     *
     * @return (optionally) a read message or none()
     *
     * @throws QException in exceptional circumstances
     */
    Optional<QMessage> claimAndResolve(QMessageQuery query) throws QException;

    /**
     * This will claim a message from the underlying {@link com.atlassian.psmq.api.queue.Queue} and mark
     * is as claimed (in the name of the {@link com.atlassian.psmq.api.QSession}'s claimant.  Once it is
     * processed then it can then be resolved
     *
     * @return (optionally) a read message or none()
     *
     * @throws QException in exceptional circumstances
     */
    Optional<QMessage> claimMessage() throws QException;

    /**
     * This will claim a message from the underlying {@link com.atlassian.psmq.api.queue.Queue} and mark
     * is as claimed (in the name of the {@link com.atlassian.psmq.api.QSession}'s claimant.  Once it is
     * processed then it can then be resolved
     *
     * @param query this is criteria for claiming messages in the queue
     *
     * @return (optionally) a claimed message or none()
     *
     * @throws QException in exceptional circumstances
     */
    Optional<QMessage> claimMessage(QMessageQuery query) throws QException;

    /**
     * This will take a previous claimed message and resolve it as processed.  It will then completely disappear
     * from the {@link com.atlassian.psmq.api.queue.Queue}.
     *
     * @param message the message to resolve as processed
     */
    void resolveClaimedMessage(QMessage message);

    /**
     * This will take a previous claimed message and unresolve it so it can be read again.  It will then be available back
     * in the {@link com.atlassian.psmq.api.queue.Queue} to be read / claimed again
     *
     * @param message the message to resolve as processed
     */
    void unresolveClaimedMessage(QMessage message);

}
