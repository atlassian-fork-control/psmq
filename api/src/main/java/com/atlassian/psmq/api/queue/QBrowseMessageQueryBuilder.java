package com.atlassian.psmq.api.queue;

import com.atlassian.annotations.PublicApi;
import com.atlassian.psmq.api.internal.Validations;
import com.atlassian.psmq.api.message.QContentType;
import com.atlassian.psmq.api.message.QContentVersion;
import com.atlassian.psmq.api.message.QMessageId;
import com.atlassian.psmq.api.message.QMessagePriority;
import com.atlassian.psmq.api.paging.DefaultPagingLimits;
import com.atlassian.psmq.api.paging.LimitedPageRequest;
import com.atlassian.psmq.api.paging.LimitedPageRequestImpl;
import com.atlassian.psmq.api.paging.PageRequest;
import com.atlassian.psmq.api.paging.SimplePageRequest;
import com.atlassian.psmq.api.property.query.QPropertyQuery;

import java.util.Optional;

/**
 * A builder of {@link QBrowseMessageQuery}s
 *
 * @since v1.0
 */
@PublicApi
public class QBrowseMessageQueryBuilder {
    Optional<QMessageId> messageId = Optional.empty();
    Optional<QContentType> contentType = Optional.empty();
    Optional<QContentVersion> version = Optional.empty();
    Optional<QMessagePriority> priority = Optional.empty();
    Optional<QPropertyQuery> properties = Optional.empty();
    PageRequest paging = new SimplePageRequest(0, DefaultPagingLimits.DEFAULT_MESSAGES_RETURNED);


    /**
     * @return a MessageQuery that finds any message, that is without criteria
     */
    public static QBrowseMessageQuery anyMessage() {
        return newQuery().build();
    }

    public static QBrowseMessageQueryBuilder newQuery() {
        return new QBrowseMessageQueryBuilder();
    }

    /**
     * This will find messages using the given message id as criteria
     *
     * @param messageId the message id in play
     *
     * @return the builder
     */
    public QBrowseMessageQueryBuilder withMessageId(QMessageId messageId) {
        this.messageId = Optional.of(messageId);
        return this;
    }

    /**
     * This will find messages using the given content type as criteria
     *
     * @param contentType the content type in play
     *
     * @return the builder
     */
    public QBrowseMessageQueryBuilder withContentType(QContentType contentType) {
        this.contentType = Optional.of(contentType);
        return this;
    }

    /**
     * This will find messages using the given content version as criteria
     *
     * @param version the content version in play
     *
     * @return the builder
     */
    public QBrowseMessageQueryBuilder withContentVersion(QContentVersion version) {
        this.version = Optional.of(version);
        return this;
    }

    /**
     * This will find messages using the given message priority as criteria
     *
     * @param priority the message priority in play
     *
     * @return the builder
     */
    public QBrowseMessageQueryBuilder withPriority(QMessagePriority priority) {
        this.priority = Optional.of(priority);
        return this;
    }

    /**
     * This will find messages using the given property query as criteria
     *
     * @param propertyQuery the message property query in play
     *
     * @return the builder
     */
    public QBrowseMessageQueryBuilder withProperties(QPropertyQuery propertyQuery) {
        this.properties = Optional.of(propertyQuery);
        return this;
    }

    /**
     * Sets the paging to use for this query
     *
     * @param paging the paging in play
     *
     * @return the builder
     */
    public QBrowseMessageQueryBuilder withPaging(PageRequest paging) {
        this.paging = Validations.checkNotNull(paging);
        return this;
    }

    /**
     * Sets the paging for this query
     *
     * @param start the page start
     * @param limit the page limit
     *
     * @return the builder
     */
    public QBrowseMessageQueryBuilder withPaging(int start, int limit) {
        this.paging = new SimplePageRequest(start, limit);
        return this;
    }


    public QBrowseMessageQuery build() {
        return new QBrowseMessageQuery() {
            @Override
            public LimitedPageRequest paging() {
                return LimitedPageRequestImpl.create(paging, DefaultPagingLimits.MAX_MESSAGES_RETURNED);
            }

            @Override
            public Optional<QMessageId> messageId() {
                return messageId;
            }

            @Override
            public Optional<QContentType> contentType() {
                return contentType;
            }

            @Override
            public Optional<QContentVersion> contentVersion() {
                return version;
            }

            @Override
            public Optional<QMessagePriority> priority() {
                return priority;
            }

            @Override
            public Optional<QPropertyQuery> properties() {
                return properties;
            }
        };
    }
}
