package com.atlassian.psmq.backdoor.internal.rest.resources;

import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.atlassian.psmq.api.QSession;
import com.atlassian.psmq.api.QSessionDefinition;
import com.atlassian.psmq.api.QSessionDefinitionBuilder;
import com.atlassian.psmq.api.QSessionFactory;
import com.atlassian.psmq.api.message.QClaimant;
import com.atlassian.psmq.api.message.QContentType;
import com.atlassian.psmq.api.message.QContentVersion;
import com.atlassian.psmq.api.message.QMessage;
import com.atlassian.psmq.api.message.QMessageBuilder;
import com.atlassian.psmq.api.message.QMessageConsumer;
import com.atlassian.psmq.api.message.QMessageId;
import com.atlassian.psmq.api.message.QMessagePriority;
import com.atlassian.psmq.api.message.QMessageProducer;
import com.atlassian.psmq.api.queue.QTopic;
import com.atlassian.psmq.api.queue.Queue;
import com.atlassian.psmq.api.queue.QueueDefinition;
import com.atlassian.psmq.api.queue.QueueDefinitionBuilder;
import org.joda.time.format.ISODateTimeFormat;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;
import java.util.Random;
import java.util.concurrent.TimeUnit;

/**
 */
@Path("/hack")
@AnonymousAllowed
public class HackResource {
    private final QSessionFactory qSessionFactory;
    private String[] messages = new String[]
            {
                    "IT is a truth universally acknowledged, that a single man in possession of a good fortune must be in want of a wife.\n"
                            + "\n"
                            + "However little known the feelings or views of such a man may be on his first entering a neighbourhood, this truth is so well fixed in the minds of the surrounding families, that he is considered as the rightful property of some one or other of their daughters.\n"
                            + "\n"
                            + "\"My dear Mr. Bennet,\" said his lady to him one day, \"have you heard that Netherfield Park is let at last?\"\n"
                            + "\n"
                            + "Mr. Bennet replied that he had not.\n"
                            + "\n"
                            + "\"But it is,\" returned she; \"for Mrs. Long has just been here, and she told me all about it.\"\n"
                            + "\n"
                            + "Mr. Bennet made no answer.\n"
                            + "\n"
                            + "\"Do not you want to know who has taken it?\" cried his wife impatiently.\n"
                            + "\n"
                            + "\"You want to tell me, and I have no objection to hearing it.\"\n"
                            + "\n"
                            + "This was invitation enough.\n"
                            + "\n"
                            + "\"Why, my dear, you must know, Mrs. Long says that Netherfield is taken by a young man of large fortune from the north of England; that he came down on Monday in a chaise and four to see the place, and was so much delighted with it that he agreed with Mr. Morris immediately; that he is to take possession before Michaelmas, and some of his servants are to be in the house by the end of next week.\""
                    ,
                    "Too hot! Hot Damned - Need a Police and Fireman - Too Hot!  Hot Damned - Make a dragon wanna retire man"
                    ,
                    "Dear future husband, here's a few things, you ought-a know, if you wanna be my one and only, all my life!"
                    ,
                    "How do you do\n"
                            + "I see you've met my faithful handyman\n"
                            + "He's just a little brought down because\n"
                            + "When you knocked\n"
                            + "He thought you were the candyman"
                    ,
                    "Cards for sorrow\n"
                            + "Cards for pain\n"
                            + "'Cause I've seen blue skies\n"
                            + "Through the tears in my eyes\n"
                            + "And I realize I'm going home"
                    ,
                    "I'll tell you once\n"
                            + "I won't tell you twice\n"
                            + "You'd better wise up, Janet Weiss\n"
                            + "Your apple pie don't taste too nice\n"
                            + "You'd better wise up, Janet Weiss\n"
            };

    public HackResource(final QSessionFactory qSessionFactory) {
        this.qSessionFactory = qSessionFactory;
    }

    @GET
    @Path("writes")
    public Response getInfo() {
        hackOnWriteAPI();
        return Response.ok("PSMQ is in da house - writing messages").type(MediaType.TEXT_PLAIN_TYPE).build();
    }

    @GET
    @Path("topics")
    public Response hackOnTopics() {
        hackOnTopicsAPI();
        return Response.ok("PSMQ is in da house - writing topics").type(MediaType.TEXT_PLAIN_TYPE).build();
    }

    private void hackOnTopicsAPI() {
        QSession qSession = createSession();

        // topic test
        QTopic topic = QTopic.newTopic("topic");

        createTopicQueue(qSession, "topicQ1", topic);
        createTopicQueue(qSession, "topicQ2", topic);
        createTopicQueue(qSession, "topicQ3", topic);

        QMessageProducer producer = qSession.createProducer(topic);

        for (int i = 0; i < rollDice(1, 5); i++) {
            QMessage msg = msg("In technical communication, topic-based authoring is a modular approach to content creation where content is structured around topics that can be mixed and reused in different contexts. It is defined in contrast with book-oriented or narrative content, written in the linear structure of written books.");

            producer.writeMessage(msg);
        }
    }

    private void hackOnWriteAPI() {
        QSession qSession = createSession();

        for (int j = 0; j < rollDice(3, 5); j++) {
            QueueDefinition testQDefinition = QueueDefinitionBuilder.newDefinition().withName(makeQName()).build();

            Queue testQueue = qSession.queueOperations().accessQueue(testQDefinition);

            QMessageProducer producer = qSession.createProducer(testQueue);

            for (int i = 0; i < rollDice(5, 20); i++) {
                QMessage msg = ranmdomMsg(i);

                producer.writeMessage(msg);
            }
        }
    }

    @SuppressWarnings("UnusedDeclaration")
    private void exampleConsumerCode() {

        QSession qSession = createSession();

        QueueDefinition qDef = QueueDefinitionBuilder.newDefinition().withName("exampleQ").build();

        Queue exampleQ = qSession.queueOperations().accessQueue(qDef);

        QMessageConsumer consumer = qSession.createConsumer(exampleQ);

        Optional<QMessage> msg = consumer.claimAndResolve();

    }

    @SuppressWarnings("UnusedDeclaration")
    private void exampleClaimConsumerCode() {

        QSession qSession = createSession();

        QueueDefinition qDef = QueueDefinitionBuilder.newDefinition().withName("webhookQ").build();
        Queue webHookQ = qSession.queueOperations().accessQueue(qDef);
        QMessageConsumer consumer = qSession.createConsumer(webHookQ);

        Optional<QMessage> optionalMsg = consumer.claimMessage();
        if (optionalMsg.isPresent()) {
            QMessage msg = optionalMsg.get();
            try {
                sendWebHook(msg);
                consumer.resolveClaimedMessage(msg);
            } catch (RuntimeException webHookProblems) {
                consumer.unresolveClaimedMessage(msg);
            }
        }
    }

    @SuppressWarnings("UnusedDeclaration")
    private void exampleTopicCode() {
        QSession qSession = createSession();

        QTopic trainTopic = QTopic.newTopic("trainSpotting");

        QueueDefinition qDef1 = QueueDefinitionBuilder.newDefinition()
                .withName("chooChooQueue")
                .withTopic(trainTopic)
                .build();
        Queue chooChooQueue = qSession.queueOperations().accessQueue(qDef1);
        QMessageConsumer chooChooConsumer = qSession.createConsumer(chooChooQueue);

        QueueDefinition qDef2 = QueueDefinitionBuilder.newDefinition()
                .withName("tootTootQueue")
                .withTopic(trainTopic)
                .build();
        Queue tootTootQueue = qSession.queueOperations().accessQueue(qDef2);
        QMessageConsumer tootTootConsumer = qSession.createConsumer(chooChooQueue);


        QMessageProducer producer = qSession.createProducer(trainTopic);
        producer.writeMessage(buildMsg("Thomas The Tank Engine"));
    }

    @SuppressWarnings("UnusedDeclaration")
    private void messageWithTheLot() {
        QMessageBuilder builder = QMessageBuilder.newMsg();
        //
        // there are some pre-build constants that you can use
        // but how you interpret then is up to you
        builder.withContentType(QContentType.JSON);
        builder.withContentVersion(QContentVersion.V0);
        //
        // in 5 days this message will expire and will no longer
        // be read from the queue.  It disappears into the aether.
        builder.withExpiryFromNow(TimeUnit.DAYS.toMillis(5));

        // it does not have to be unique - that's up to you
        // you also don't have to use it.  One will be generated
        // if you don't specify one
        builder.withId(QMessageId.id("someIdOfYourChoosing"));

        //
        // Properties have names and types and can be strings, longs
        // dates or booleans
        builder.withProperty("firstNamedProperty", "stringValue")
                .withProperty("secondNamedProperty", 666)
                .withProperty("thirdNamedProperty", new Date())
                .withProperty("fourthNamedProperty", true);

        //
        // You can use QBuffer to append any sort of data into the message.  Go wild!
        builder.newBuffer()
                .append("Some")
                .append("message")
                .append("data")
                .append("including some specific types such as")
                .append(5f).append(5).append(5L)
                .append(true)
                .append(" and so on.  Read the JavaDoc for more examples!")
                .buildBuffer();

        QMessage msg = builder.build();
    }

    @SuppressWarnings({"UnusedDeclaration", "ConstantConditions", "StatementWithEmptyBody"})
    private void claimantExample() {
        QClaimant claimant = QClaimant.claimant("myNameThatMeansSomeToMe");
        QSessionDefinition sessionDefinition = QSessionDefinitionBuilder.newDefinition().withAutoCommitStrategy().withClaimant(claimant).build();
        QSession qSession = qSessionFactory.createSession(sessionDefinition);


        qSession.queueOperations().unresolveAllClaimedMessages();

        Queue someQ = qSession.queueOperations().accessQueue(QueueDefinitionBuilder.newDefinition().withName("someQ").build());
        qSession.queueOperations().unresolveAllClaimedMessages(someQ);


        QClaimant nodeClaimant = QClaimant.claimant("outbound_email_processor");
        QSession qMailSession = qSessionFactory.createSession(QSessionDefinitionBuilder.newDefinition()
                .withClaimant(claimant).build());
        Optional<Queue> outboundQ = qMailSession.queueOperations().exclusiveAccessQueue(QueueDefinitionBuilder.newDefinition().withName("outboundQ").build());
        if (outboundQ.isPresent()) {
            // right we go exclusive access - lets process things
            processOutboundEmails(qMailSession, outboundQ.get());
        }

    }

    @SuppressWarnings("UnusedParameters")
    private void processOutboundEmails(final QSession qMailSession, final Queue queue) {
        //...
    }

    private QMessage buildMsg(final String s) {
        throw new UnsupportedOperationException("Not implemented");
    }

    private void sendWebHook(final QMessage msg) {
        throw new UnsupportedOperationException("Not implemented");
    }

    private QSession createSession() {
        QSessionDefinition sessionDefinition = QSessionDefinitionBuilder.newDefinition()
                .withAutoCommitStrategy()
                .build();
        return qSessionFactory.createSession(sessionDefinition);
    }

    private Queue createTopicQueue(final QSession qSession, String name, final QTopic topic) {
        QueueDefinition testQDefinition = QueueDefinitionBuilder.newDefinition()
                .withName(name)
                .withTopic(topic)
                .withProperty("someString", "someValue")
                .withProperty("someInt", 666)
                .build();
        return qSession.queueOperations().accessQueue(testQDefinition);
    }

    private int rollDice(final int low, final int highExclusive) {
        int r = new Random().nextInt((highExclusive) - low);
        return low + r;
    }

    private String makeQName() {
        String yyyy_mm_sss = new SimpleDateFormat("yyyy_hh_mm_sss").format(new Date());
        return String.format("hackQ_%s", yyyy_mm_sss);
    }

    private QMessage msg(String content) {
        QMessageBuilder builder = QMessageBuilder.newMsg()
                .newBuffer().append(content).append(" @ ").append(nowStr())
                .buildBuffer()
                .withPriority(QMessagePriority.NORMAL);
        return builder.build();

    }

    private QMessage ranmdomMsg(final int i) {

        int index = rollDice(0, messages.length);
        String msg = messages[index];
        QMessageBuilder builder = QMessageBuilder.newMsg()
                .newBuffer().append("#").append(i).append(" ").append(msg).append(" @ ").append(nowStr())
                .buildBuffer()
                .withPriority(QMessagePriority.NORMAL);


        int numProps = rollDice(0, 5);
        for (int j = 0; j < numProps; j++) {
            int type = rollDice(0, 4);
            if (type == 0) {
                builder.withProperty("stringProp", "String : " + nowStr());
            }
            if (type == 1) {
                builder.withProperty("longProp", 666);
            }
            if (type == 2) {
                builder.withProperty("dateProp", new Date());
            }
            if (type == 3) {
                builder.withProperty("booleanProp", rollDice(0, 100) % 2 == 0);
            }
        }

        return builder
                .build();
    }

    private String nowStr() {
        return ISODateTimeFormat.dateTime().print(System.currentTimeMillis());
    }
}
