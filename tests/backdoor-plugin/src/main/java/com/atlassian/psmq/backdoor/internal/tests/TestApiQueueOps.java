package com.atlassian.psmq.backdoor.internal.tests;

import com.atlassian.psmq.api.QException;
import com.atlassian.psmq.api.QSession;
import com.atlassian.psmq.api.message.QClaimant;
import com.atlassian.psmq.api.message.QContentType;
import com.atlassian.psmq.api.message.QMessage;
import com.atlassian.psmq.api.message.QMessageConsumer;
import com.atlassian.psmq.api.message.QMessageProducer;
import com.atlassian.psmq.api.queue.QTopic;
import com.atlassian.psmq.api.queue.Queue;
import com.atlassian.psmq.api.queue.QueueDefinition;
import com.atlassian.psmq.api.queue.QueueDefinitionBuilder;
import org.junit.Test;

import java.util.Date;
import java.util.Optional;

import static com.atlassian.psmq.backdoor.internal.tests.Fixtures.basicMsg;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

/**
 */
public class TestApiQueueOps extends BaseApiTest {
    public static final String SEE_WHATS_ON_THE_SLAB = "See whats on the slab";


    @Test(expected = QException.class)
    public void test_accessing_existing_queue_with_different_shape() {
        // assemble
        String qName = Fixtures.rqName();

        QueueDefinition initialDef = QueueDefinitionBuilder.newDefinition()
                .withName(qName)
                .withPurpose("To save the world")
                .build();
        QueueDefinition sameButDifferent = QueueDefinitionBuilder.newDefinition()
                .withName(qName)
                .withTopic(QTopic.newTopic("worldChanging"))
                .withPurpose("To save the world")
                .build();


        // act
        qSession.queueOperations().accessQueue(initialDef);
        //
        // this should throw exception here
        qSession.queueOperations().accessQueue(sameButDifferent);
    }

    @Test
    public void test_basic_queue_properties() {
        // assemble
        Date nowDate = new Date(now);
        String qName = Fixtures.rqName();
        QueueDefinition queueDef = QueueDefinitionBuilder.newDefinition()
                .withName(qName)
                .withProperty("stringProperty", "someString")
                .withProperty("dateProperty", nowDate)
                .withProperty("longProperty", 666L)
                .withProperty("booleanProperty", false)
                .build();

        // act
        Queue queue = qSession.queueOperations().accessQueue(queueDef);

        // assert
        assertThat(queue, notNullValue());
        assertThat(queue.properties().names().size(), equalTo(4));
        assertThat(queue.properties().stringValue("stringProperty").get(), equalTo("someString"));
        assertThat(queue.properties().dateValue("dateProperty").get(), equalTo(nowDate));
        assertThat(queue.properties().longValue("longProperty").get(), equalTo(666L));
        assertThat(queue.properties().booleanValue("booleanProperty").get(), equalTo(false));
    }

    @Test
    public void test_basic_queue_write_and_read() {
        // assemble
        String qName = Fixtures.rqName();
        QueueDefinition queueDef = QueueDefinitionBuilder.newDefinition()
                .withName(qName)
                .build();

        // act
        Queue queue = qSession.queueOperations().accessQueue(queueDef);
        QMessageProducer messageProducer = qSession.createProducer(queue);
        long writtenCount = messageProducer.writeMessage(basicMsg(SEE_WHATS_ON_THE_SLAB));

        QMessageConsumer messageConsumer = qSession.createConsumer(queue);
        Optional<QMessage> readMsg = messageConsumer.claimAndResolve();
        Optional<QMessage> nextMsg = messageConsumer.claimAndResolve();

        // assert
        assertThat(writtenCount, equalTo(1L));
        assertThat(readMsg.isPresent(), equalTo(true));
        assertThat(nextMsg.isPresent(), equalTo(false));

        QMessage msg = readMsg.get();
        assertThat(msg.buffer().asString(), equalTo(SEE_WHATS_ON_THE_SLAB));
        assertThat(msg.contentType(), equalTo(QContentType.PLAIN_TEXT));
        assertThat(msg.buffer().length(), equalTo((long) SEE_WHATS_ON_THE_SLAB.length()));
        assertThat(msg.systemMetaData().createdDate().getTime() >= now, equalTo(true));
        assertThat(msg.expired(), equalTo(false));
        assertThat(msg.expiryDate().isPresent(), equalTo(false));
        assertThat(msg.messageId(), notNullValue());
    }

    @Test
    public void test_queue_locking() throws Exception {
        // assemble
        QueueDefinition queueDef = QueueDefinitionBuilder.newDefinition().withName(Fixtures.rqName()).build();

        QSession firstSession = qSession;
        QSession secondSession = psmqFixture.autoCommitSession();

        // act
        Optional<Queue> q1 = firstSession.queueOperations().exclusiveAccessQueue(queueDef);
        Optional<Queue> q2 = secondSession.queueOperations().exclusiveAccessQueue(queueDef);

        // assert
        assertThat(q1.isPresent(), equalTo(true));
        assertThat(q2.isPresent(), equalTo(false));
    }

    @Test
    public void test_queue_locking_and_releasing() throws Exception {
        // assemble
        QueueDefinition queueDef = QueueDefinitionBuilder.newDefinition().withName(Fixtures.rqName()).build();

        QSession firstSession = qSession;
        QSession secondSession = psmqFixture.autoCommitSession();

        // act
        Optional<Queue> q1 = firstSession.queueOperations().exclusiveAccessQueue(queueDef);
        Optional<Queue> q2 = secondSession.queueOperations().exclusiveAccessQueue(queueDef);

        firstSession.queueOperations().releaseQueue(q1.get());
        Optional<Queue> q3 = secondSession.queueOperations().exclusiveAccessQueue(queueDef);

        // assert
        assertThat(q1.isPresent(), equalTo(true));
        assertThat(q2.isPresent(), equalTo(false));
        assertThat(q3.isPresent(), equalTo(true));
    }

    @Test
    public void test_queue_locking_and_releasing_with_timeout() throws Exception {
        // assemble
        QueueDefinition queueDef = QueueDefinitionBuilder.newDefinition().withName(Fixtures.rqName()).build();

        QSession firstSession = psmqFixture.autoCommitSessionWithClaimant(QClaimant.claimant("first"), 1000);
        QSession secondSession = psmqFixture.autoCommitSessionWithClaimant(QClaimant.claimant("second"), 1000);

        // act
        Optional<Queue> q1 = firstSession.queueOperations().exclusiveAccessQueue(queueDef);

        Thread.sleep(100);
        Optional<Queue> q2 = secondSession.queueOperations().exclusiveAccessQueue(queueDef);

        Thread.sleep(100);
        Optional<Queue> q3 = secondSession.queueOperations().exclusiveAccessQueue(queueDef);

        Thread.sleep(1200);
        Optional<Queue> q4 = secondSession.queueOperations().exclusiveAccessQueue(queueDef);

        // assert
        assertThat(q1.isPresent(), equalTo(true));
        assertThat(q2.isPresent(), equalTo(false));
        assertThat(q3.isPresent(), equalTo(false));
        assertThat(q4.isPresent(), equalTo(true));
    }

    @Test
    public void releaseIfEmpty__can_release_when_queue_is_empty() throws Exception {
        QueueDefinition queueDef = QueueDefinitionBuilder.newDefinition().withName(Fixtures.rqName()).build();

        QSession session = psmqFixture.autoCommitSession();

        Queue queue = session.queueOperations().exclusiveAccessQueue(queueDef).get();

        boolean released = session.queueOperations().releaseQueueIfEmpty(queue);
        assertTrue(released);
    }

    @Test
    public void releaseIfEmpty__cannot_release_when_queue_is_not_empty() throws Exception {
        QueueDefinition queueDef = QueueDefinitionBuilder.newDefinition().withName(Fixtures.rqName()).build();

        QSession session = psmqFixture.autoCommitSession();

        Queue queue = session.queueOperations().exclusiveAccessQueue(queueDef).get();

        QMessageProducer producer = session.createProducer(queue);
        producer.writeMessage(basicMsg(SEE_WHATS_ON_THE_SLAB));

        boolean released = session.queueOperations().releaseQueueIfEmpty(queue);
        assertFalse(released);
    }
}
