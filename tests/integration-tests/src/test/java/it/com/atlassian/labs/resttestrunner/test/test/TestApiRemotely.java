package it.com.atlassian.labs.resttestrunner.test.test;

import ivmtestrunner.api.client.JunitClientSideRemoteTestRunner;
import ivmtestrunner.api.client.TestResource;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 */
@RunWith (JunitClientSideRemoteTestRunner.class)
@TestResource(url = "http://localhost:2990/jira/rest/psmqbackdoor/tests", suiteInvoker = true)
public class TestApiRemotely
{
    @Test
    public void testBasic() throws Exception
    {
    }
}
